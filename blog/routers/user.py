from fastapi import Depends,APIRouter
from .. import schemas, database, models
from sqlalchemy.orm import Session
get_db = database.get_db
from ..repository import user

router = APIRouter(
    prefix="/user",
    tags=['User'])

@router.post('/',response_model=schemas.ShowUser)
def create_user(request:schemas.User,db: Session = Depends(get_db)):
    return user.create(request,db)

@router.get('/',response_model=schemas.ShowUser)
def get_user(id:int,db: Session = Depends(get_db)):
    return user.show(id,db)